import React from 'react';
import ReactJson from 'react-json-view'

import { useJson } from '../../context/json';

const Main: React.FC = () => {
  const { json } = useJson();  

  return (
    <div>
      <h1>Json</h1>
        <ReactJson style={{ textAlign: 'start' }} src={json}/>
    </div>
  );
}

export default Main;