import React, { useState, useEffect } from 'react';
import ReactLoading from 'react-loading';

import './styles.css';

import receiptImg from '../../assets/billet.svg';
import cardImg from '../../assets/credit-card.svg';
import billetImg from '../../assets/receipt.svg';
import api from '../../services/api';
import { useJson } from '../../context/json';

interface BilletDataProps {
  settleDate: string;
}

const Main: React.FC = () => {
  
  const [digitable, setDigitable] = useState('');
  const [billetData, setBilletData] = useState<BilletDataProps>();
  const [id, setId] = useState('');
  
  const [error, setError] = useState('');
  const [billetStatus, setBilletStatus] = useState('');
  const [billetMessage, setBilletMessage] = useState('');
  const [showCardInputs, setShowCardInputs] = useState(true);
  
  const [inputValue, setInputValue] = useState('');
  
  const [inuptDisabled, setInuptDisabled] = useState(true);
  const [showBilletPanel, setShowBilletPanel] = useState(true);
  const [showCardPanel, setShowCardPanel] = useState(false);
  const [showButtonPay, setShowButtonPay] = useState(false);
  const [showButtonCancel, setShowButtonCancel] = useState(false);
  const [loading, setLoading] = useState(false);

  const [cardTitle, setCardTitle] = useState('Boleto');
  const [cardImage, setCardImage] = useState(billetImg);

  const { addJson, handleAddBilletContext } = useJson();

  useEffect(() => {console.log(billetStatus)}, [billetStatus]);
  const handleBillet = async () => {
    setError('');
    setLoading(true);
    setShowButtonPay(true);
    const response = await api.post('/orderdataitembillets/inputBillet', {
      digitable,
    });
    setLoading(false);

    if (response.data.responseJson.errorCode !== '000') {
      setBilletStatus(response.data.responseJson.status);
      setBilletMessage(response.data.responseJson.message);
      setError(response.data.responseJson.message);
      addJson(response.data.responseJson);
      return;
    }

    if(response.data.responseJson.registerData) {
      setInuptDisabled(false);
    }

    addJson(response.data.responseJson);
    setBilletData(response.data.responseJson);
    setInputValue(response.data.responseJson.value);
    setId(response.data.responseJson.message);
  };

  const handleAddBillet = async () => {
    if(error) {
      setError('');
      addJson('');
      setDigitable('');
      return;
    }
    
    const response = await handleAddBilletContext(id);

    setCardTitle('Resumo');
    setCardImage(cardImg)
    setShowButtonCancel(true);
    
    if (
      response.data.responseJson[0].itemBillet.return.returnCode !== '002'
      ) {
        setError(response.data.responseJson.message);
      } else {
        setShowBilletPanel(false);
        setShowCardPanel(true);
      }

      setId(response.data.responseJson[0].itemBillet.itemId) 
    
      setBilletStatus(response.data.responseJson[0].itemBillet.return.status);
      setBilletMessage(response.data.responseJson[0].itemBillet.return.message);
  };
    
  const handlePayment = async () => { 
      
      const response = await api.post('/orderdataitembillets/sagaSettleBillet',{
        itemId: [id],
      });
      
      if (
        response.data.responseJson[0].itemBillet.return.returnCode !== '10000'
      ) {
          setError(response.data.responseJson[0].itemBillet.return.message);
        } else {
          setShowBilletPanel(false);
          setShowCardPanel(true);
          setShowButtonCancel(false);
          setCardTitle('Pagamento')
          setCardImage(receiptImg);
        }
        
    setShowCardInputs(false);
    setBilletStatus(response.data.responseJson[0].itemBillet.return.status);
    setBilletMessage(response.data.responseJson[0].itemBillet.return.message);
  };

  const cancelPaymentBillet = async () => { 
      
    const response = await api.post('/orderdataitembillets/sagaSettleBillet',{
      itemId: [id],
    });
    
    if (
      response.data.responseJson[0].itemBillet.return.returnCode !== '10000'
    ) {
        setError(response.data.responseJson[0].itemBillet.return.message);
      } else {
        setShowBilletPanel(false);
        setShowCardPanel(true);
        setShowButtonCancel(false);
        setCardTitle('Pagamento')
        setCardImage(receiptImg);
      }
      
  setShowCardInputs(false);
  setBilletStatus(response.data.responseJson[0].itemBillet.return.status);
  setBilletMessage(response.data.responseJson[0].itemBillet.return.message);
};

  return (
    <div className="main">
      <div className="inner">
      {showBilletPanel && (
        <>
          <div>
          <img src={cardImage} alt="Credit Card"/>
          <h1>{cardTitle}</h1>

            <input 
              placeholder="Digite o códgio"
              className="main--input" 
              value={digitable}
              onChange={text => setDigitable(text.target.value)}
            />

                {billetStatus && ( 
                  <p>
                    <span style={{ color: 'red', fontWeight: 'bold' }}>{billetStatus}</span>
                    <br />
                    <span style={{ color: 'red', fontWeight: 'bold' }}>{billetMessage}</span> 
                  </p>
                )}

            {loading && (
              <span className="loading">
                  <ReactLoading type={'spin'} color={'#325A9D'} width={50} height={50} />
              </span>
            )}
          </div>

          <div>
            {billetData?.settleDate && (
              <>
                <p>Valor:</p>
                <input
                  disabled={inuptDisabled}
                  value={inputValue}  
                  onChange={text => {
                    if (Number(text.target.value) < 1 || Number(text.target.value) > 9999999) {
                      setError('Valor inválido')
                    } else {
                      setError('')
                    }
                    setInputValue(text.target.value)
                  }}
                />
                <p>Data de liquidação:</p>
                <p>{billetData.settleDate && billetData.settleDate}</p>


                {error && <span style={{ color: 'red', fontWeight: 'bold' }}>{error}</span>}
                {billetStatus && ( 
                  <p>
                    <span style={{ color: 'red', fontWeight: 'bold' }}>{billetStatus}</span>
                    <br />
                    <span style={{ color: 'red', fontWeight: 'bold' }}>{billetMessage}</span> 
                  </p>
                )}
              </>
            )}
          </div>
        </>
      )}

      {showCardPanel  && (
        <>
          <img src={cardImage} alt="Credit Card"/>
          <h1>{cardTitle}</h1>

          {billetStatus && showCardInputs && (
            <div className="card-credentials">
              <input placeholder="N° do cartão" />

              <input placeholder="Validade" />

              <input placeholder="Código" />
            </div>
          )}

          {billetStatus && ( 
            <p>
              Status do boleto:
              <br />
              <span style={{ color: 'orange', fontWeight: 'bold' }}>{billetStatus}</span>

              <br />

              Message:
              <br />
              <span style={{ color: 'orange', fontWeight: 'bold' }}>{billetMessage}</span> 
            </p>
          )}
        </>
      )}

      {!error && !showButtonPay && <button onClick={handleBillet}>Consultar</button> }

      {!error && showButtonPay && (
        <button onClick={ showBilletPanel ? handleAddBillet : handlePayment}>
          { showBilletPanel ? 'Confirmar' : 'Pagar' }
        </button>
      )}

      {!error && showButtonCancel && (
        <button onClick={cancelPaymentBillet}>
          Cancelar
        </button>
      )}


      {error && (
        <button onClick={() => {
          setError('');
          setBilletStatus('');
          setBilletMessage('');
          setDigitable('');
          setCardTitle('Boleto');
          setBilletData({} as BilletDataProps);
          setShowButtonPay(false);
          setShowBilletPanel(true);
          setShowCardPanel(false);
          setShowButtonCancel(false);
          setCardImage(billetImg);
          addJson();
        }}>Voltar</button>
      )}
    </div>

    </div>
  );
}

export default Main;