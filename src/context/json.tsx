import React, { createContext, useState, useContext } from 'react';

import api from '../services/api';

interface JsonContextData {
  json: {};
  addJson(newJson?: string): void;
  handleAddBilletContext(id: string): Promise<any>;
}

const JsonContext = createContext<JsonContextData>({} as JsonContextData);

const JsonProvider: React.FC  = ({ children }) =>  {
  const [json, setJson] = useState({});

  const addJson = (newJson: string) => {
    console.log(newJson);
    setJson(newJson);
  }

  const handleAddBilletContext = async (id: string) => {
    const response = await api.post('/orderdataitembillets/sagaSettleBillet',{
      itemId: [id],
    });

    addJson(response.data.responseJson[0].itemBillet);

    return response;
  }
 
  return (
    <JsonContext.Provider value={{ addJson, json, handleAddBilletContext }}>
      {children}
    </JsonContext.Provider>
  )
}

function useJson() {
  const context = useContext(JsonContext);

  if(!context) {
    throw new Error('useJson must be used within JsonProvider');
  }

  return context;
}

export { useJson, JsonProvider }