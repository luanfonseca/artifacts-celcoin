import React from 'react';

import './css/global.css';

import Main from './components/Main';
import PrettyJson from './components/PrettyJson';

import { JsonProvider } from './context/json';

function App() {

  return (
    <div className="app">
      <JsonProvider>
        <Main />
        <PrettyJson />
      </JsonProvider>
    </div>
  );
  
}

export default App;
